#!venv/bin/python3
from telegram.ext import Updater, CommandHandler, PollAnswerHandler

import database_script
import logging

from Lecture import Lecture, russian_days
from datetime import datetime, timezone
import pytz
import random
import time

tz = pytz.timezone('Europe/Moscow')


lectures = dict()
rooms = dict()

master = 416423740


def start(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text='Привет, пожалуйста, один '
                             'раз аккуратно добавь через /fio свои ФИО (или просто фамилию и имя),'
                             ' чтобы я мог тебя записать в списки.\n'
                             'Дай команде /go циферку от 1 до 5 (день недели), чтобы записаться на курсы этого дня')
    database_script.add_user_to_db(update.effective_chat.id, update.effective_chat.username)


def get_name(update, context):
    if update.effective_chat.id != master:
        return
    context.bot.send_message(chat_id=master, text=database_script.get_username(context.args[0]))


def lottery(update, context):
    if update.effective_chat.id != master:
        return

    random.seed()

    user = dict()
    for lecture in lectures.values():
        lecture.lucky.clear()
        lecture.loser.clear()

        number = rooms[lecture.room]
        arr = list(lecture.wait)
        random.shuffle(arr)

        lecture.lucky = set(arr[:number])
        for el in lecture.wait:
            if el not in user:
                user[el] = []
            if el not in lecture.lucky:
                lecture.loser.add(el)
            else:
                user[el].append(lecture.name)
        lecture.wait.clear()

    for chat_id, names in user.items():
        chat_id = int(chat_id)
        if not names:
            context.bot.send_message(chat_id=chat_id, text="Не повезло, не повезло...")
        else:
            context.bot.send_message(chat_id=chat_id, text="Вот, что выдало колесо фортуны:\n\n" + '\n'.join(names))


def write_users_from_queue(q, f, name):
    f.write(''.join([name, ':', '\n']))
    for chat_id in q:
        username = database_script.get_username(chat_id)
        f.write(''.join([username, ' ', str(chat_id), '\n']))


def info(update, context):
    if update.effective_chat.id != master:
        return
    with open("info.txt", "w") as f:
        for lecture in lectures.values():
            f.write(''.join([lecture.name, '.', russian_days[lecture.day], '\n']))
            write_users_from_queue(lecture.wait, f, "Ждут")
            write_users_from_queue(lecture.lucky, f, "Повезло, повезло")
            write_users_from_queue(lecture.loser, f, "Не повезло, не повезло")
            lecture.loser -= lecture.lucky
            f.write('\n')


def fio(update, context):
    name = ' '.join(context.args)
    result = database_script.change_username(update.effective_chat.id, name)

    if result:
        text = 'Я добавил твои ФИО в базу данных'
    else:
        text = 'Что-то пошло не так...'
    context.bot.send_message(chat_id=update.effective_chat.id, text=text)


def create_init_tables():
    database_script.create_log_table()
    database_script.create_user_table()


def go(update, context) -> None:
    global lectures, rooms

    day = int(context.args[0]) - 1

    questions = [name for name in lectures.keys() if lectures[name].day == day]
    message = context.bot.send_poll(
        update.effective_chat.id,
        "Курсы. " + russian_days[day],
        questions,
        is_anonymous=False,
        allows_multiple_answers=True,
        timeout=300
    )

    payload = {
        message.poll.id: {
            "questions": questions,
            "message_id": message.message_id,
            "chat_id": update.effective_chat.id,
            "day": day,
            "name": "go",
        }
    }
    context.bot_data.update(payload)


def give(update, context) -> None:
    global lectures, rooms

    day = int(context.args[0]) - 1

    questions = [name for name in lectures.keys() if lectures[name].day == day]
    message = context.bot.send_poll(
        update.effective_chat.id,
        "Отдаю курсы. " + russian_days[day],
        questions,
        is_anonymous=False,
        allows_multiple_answers=True,
        timeout=300
    )

    payload = {
        message.poll.id: {
            "questions": questions,
            "message_id": message.message_id,
            "chat_id": update.effective_chat.id,
            "day": day,
            "name": "give",
        }
    }
    context.bot_data.update(payload)


def call_go(context, poll_id, chat_id, questions, selected_options):
    time = datetime.now(tz)
    weekday = int(time.strftime("%w"))
    day = int(context.bot_data[poll_id]["day"])

    log = " ".join(["Add", str(chat_id), str(day), ",".join([str(opt) for opt in selected_options])])
    print(log)
    database_script.add_log_message(str(time), log)

    if (weekday != 5 and weekday != 6) or (weekday == 5 and time.hour < 10) or (weekday == 6 and time.hour > 12):
        for option in selected_options:
            name = questions[option]
            if len(lectures[name].lucky) < rooms[lectures[name].room]:
                lectures[name].lucky.add(chat_id)
                context.bot.send_message(
                    chat_id,
                    text="Добавил на курс: " + name
                )
            else:
                lectures[name].loser.add(chat_id)

        context.bot.send_message(
            chat_id,
            text="Записал в очередь для запасных мест, там где не удалось добавить"
        )
        context.bot.stop_poll(
            chat_id, context.bot_data[poll_id]["message_id"]
        )
        return

    for question in questions:
        if chat_id in lectures[question].wait:
            lectures[question].wait.remove(chat_id)

    for option in selected_options:
        name = questions[option]
        lectures[name].wait.add(chat_id)

    context.bot.send_message(
        context.bot_data[poll_id]["chat_id"],
        text="Записал в основную очередь на удачу на следующую неделю"
    )
    context.bot.stop_poll(
        chat_id, context.bot_data[poll_id]["message_id"]
    )


def call_give(context, poll_id, chat_id, questions, selected_options):
    time = datetime.now(tz)
    weekday = int(time.strftime("%w"))
    day = int(context.bot_data[poll_id]["day"])

    log = " ".join(["Delete", str(chat_id), str(day), ",".join([str(opt) for opt in selected_options])])
    print(log)
    database_script.add_log_message(str(time), log)

    if time.hour < 10 or time.hour > 22:
        context.bot.send_message(
            chat_id,
            text="Не буди людей, пожалуйста, отложенная очередь еще не реализована)"
        )
        context.bot.stop_poll(
            chat_id, context.bot_data[poll_id]["message_id"]
        )
        return

    if weekday != 6 and (day + 1 < weekday or (day + 1 == weekday and time.hour > 18)):
        context.bot.send_message(
            chat_id,
            text="Поздно, время ушло("
        )
        context.bot.stop_poll(
            chat_id, context.bot_data[poll_id]["message_id"]
        )
        return

    for option in selected_options:
        question = questions[option]
        if chat_id in lectures[question].lucky:
            lectures[question].lucky.remove(chat_id)
            nl = list(lectures[question].loser)
            if nl:
                nl = nl[random.randint(0, len(nl) - 1)]
                lectures[question].loser.remove(nl)
                lectures[question].lucky.add(nl)

                context.bot.send_message(
                    chat_id=nl,
                    text="Кто-то отдал тебе место на курс: " + question
                )

    context.bot.send_message(
        context.bot_data[poll_id]["chat_id"],
        text="Освободил твои места, если они были :)"
    )
    context.bot.stop_poll(
        chat_id, context.bot_data[poll_id]["message_id"]
    )


def receive_answer(update, context) -> None:
    answer = update.poll_answer
    poll_id = answer.poll_id

    try:
        chat_id = context.bot_data[poll_id]["chat_id"]
    except KeyError:
        return

    selected_options = answer.option_ids
    questions = context.bot_data[poll_id]["questions"]

    if context.bot_data[poll_id]["name"] == "go":
        call_go(context, poll_id, chat_id, questions, selected_options)
    elif context.bot_data[poll_id]["name"] == "give":
        call_give(context, poll_id, chat_id, questions, selected_options)


def init_from_file():
    cur = None
    n = 0

    with open("info.txt") as f:
        for line in f:
            if cur is None:
                cur = '.'.join(line.split('.')[:-1])
                continue
            if line == '\n':
                cur = None
                n = 0
                continue

            ar = line[:-1].split(' ')[-1]
            if ar.isnumeric():
                if n == 1:
                    lectures[cur].wait.add(ar)
                elif n == 2:
                    lectures[cur].lucky.add(ar)
                elif n == 3:
                    lectures[cur].loser.add(ar)
                continue

            if ':' in line:
                n += 1
                continue


def read_lectures():
    with open("lectures.txt") as f:
        for line in f:
            line = line.split(';')
            for i in range(len(line)):
                line[i] = line[i].strip()
            lectures[line[0]] = Lecture(*line)


def read_rooms():
    with open("rooms.txt") as f:
        for line in f:
            line = line.split(' ')
            for i in range(len(line)):
                line[i] = line[i].strip()
            rooms[line[0]] = int(line[1])


def main():
    global lectures, rooms
    random.seed(time.time())

    create_init_tables()
    read_lectures()
    read_rooms()
    init_from_file()

    token = open('token').read()
    if token[-1] == '\n':
        token = token[:-1]
    updater = Updater(token)
    dp = updater.dispatcher
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        level=logging.INFO)

    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("fio", fio))
    dp.add_handler(CommandHandler("go", go))
    dp.add_handler(CommandHandler("give", give))
    dp.add_handler(PollAnswerHandler(receive_answer))
    dp.add_handler(CommandHandler("get_name", get_name))
    dp.add_handler(CommandHandler("info", info))
    dp.add_handler(CommandHandler("lottery", lottery))

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
