russian_days = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница']


class Lecture:
    def __init__(self, name, room, day, time):
        self.name = name
        self.room = room
        self.day = russian_days.index(day)
        self.time = time
        self.wait = set()
        self.lucky = set()
        self.loser = set()
