import sqlite3
import functools


def connection_to_database(func):
    @functools.wraps(func)
    def wrapper(*args):
        conn = sqlite3.connect('ysda.db')
        with conn:
            cur = conn.cursor()
            answer = func(conn, cur, *args)
        conn.close()
        return answer
    return wrapper


@connection_to_database
def create_user_table(conn, cur):
    cur.execute('''
        CREATE TABLE IF NOT EXISTS users (
            id INT PRIMARY KEY,
            name VARCHAR(255)
        );
    ''')
    conn.commit()


@connection_to_database
def create_log_table(conn, cur):
    cur.execute('''
        CREATE TABLE IF NOT EXISTS log (
            time VARCHAR(255),
            message TEXT
        );
    ''')
    conn.commit()


@connection_to_database
def drop_lecture_table(conn, cur):
    cur.execute('''
        DROP TABLE IF EXISTS lectures;
    ''')
    conn.commit()


@connection_to_database
def create_lecture_table(conn, cur):
    cur.execute('''
        CREATE TABLE IF NOT EXISTS lectures (
            name VARCHAR(255) PRIMARY KEY,
            location VARCHAR(255),
            number INT
        );
    ''')
    conn.commit()


@connection_to_database
def add_lecture(conn, cur, name, location, number):
    query = "INSERT INTO lectures(name, location, number) VALUES (\"{0}\", \"{1}\", {2});".format(name, location, number)
    cur.execute(query)
    conn.commit()


@connection_to_database
def add_user_to_db(conn, cur, chat_id, username):
    """Add user to database if he is new"""
    try:
        query = "INSERT INTO users(id, name) VALUES ({0}, \"{1}\");".format(chat_id, username)
        cur.execute(query)
        conn.commit()
        return True
    except Exception:
        return False


@connection_to_database
def add_log_message(conn, cur, time, text):
    """Add user to database if he is new"""
    try:
        query = "INSERT INTO log(time, text) VALUES (\"{0}\", \"{1}\");".format(time, text)
        cur.execute(query)
        conn.commit()
        return True
    except Exception:
        return False


@connection_to_database
def change_username(conn, cur, chat_id, username):
    """Change username"""
    try:
        query = "UPDATE users SET name = \"{1}\" WHERE id = {0};".format(chat_id, username)
        cur.execute(query)
        conn.commit()
        return True
    except Exception:
        return False


@connection_to_database
def get_username(conn, cur, chat_id):
    """Change username"""
    query = "SELECT name FROM users WHERE id = {0};".format(chat_id)
    cur.execute(query)
    answer = "NIKTO"
    for row in cur.fetchall():
        answer = str(row[0])
        break
    conn.commit()
    return answer
